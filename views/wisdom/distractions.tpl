<article>
  <div>
    <h2>Working with Distractions</h2>

    <p>The members of my SRE team were asked if they were more effective when multitasking or during periods of focus. Unsurprisingly, the group concluded that focused work was more productive but not feasible daily. </p>

    <p>The daily barrage of ad-hoc work, short-term tasks, and meetings can feel overwhelming. Meanwhile, finding time to do meaningful, valuable and enjoyable work seems impossible. </p>

    <p>Business slows for no one, so you must find a way to do more with less time to achieve meaningful work. The good news is this is entirely doable. I bet you’ve experienced being more productive in a tremendous thirty-minute block than an entire week of business as usual. While cutting out four hours of focus time might not be possible, finding thirty minutes daily to focus is reasonable. </p>

    <figure class="image">
      <img src="https://res.cloudinary.com/boat-shed/image/upload/v1696202876/meditation_lz7hvb.webp" alt="rocks stacked artisticlly on a beach" />
      <figcaption>Chaos at work makes it hard to find peace outside of work.</figcaption>
    </figure>

    <h2>Understanding the Creative Process</h2>

    <p>Understanding the creative process can help us become more productive and resilient to distractions. The <em>creative process</em> is an iteration between two modes of work: the divergent mode and the convergent mode. In the <em>divergent mode</em>, you collect as many resources as possible; you should follow links, go down rabbit holes and explore a wide range of ideas. In the <em>convergent mode</em>, you refine the resources and thoughts you collected in the divergent mode. The convergent mode is where the observable output is created. </p>

    <blockquote cite="https://www.buildingasecondbrain.com/book">Our knowledge is our most valuable asset, and our ability to direct our attention is our most valuable skill<br /><br />
      - Tiago Forte, <cite>Building a Second Brain</cite>
    </blockquote>

    <p>The critical insight is that once we take control of which mode we are working in, we can produce great work in short periods. We can prepare for focused productivity while in a distraction-rich environment. </p>

    <p>It is essential to recognise that most of your time will be in divergent mode throughout the day. That’s okay; this is a valuable time for setting yourself up for productivity later. During this time, you should be collecting the resources you will need, fostering thoughts and ideas about how the work should be done, and figuring out how to make the unit of work as small, isolated and reusable as possible. Whatever you do, don’t try to get work done at this stage. Trying to do convergent work with distractions and interruptions leads to frustration and mediocre quality. </p>

    <blockquote cite="https://www.buildingasecondbrain.com/book">When the opportunity arrives to do our best work, it’s not the time to start reading books and doing research. You need that research to already be done.<br /><br />
      - Tiago Forte, <cite>Building a Second Brain</cite>
    </blockquote>

    <p>You will only need a little focused time when it comes time to get work done. You’ll be more productive in a focused thirty minutes than a full day of context-switching. Your colleges will be much more likely to support you by not distracting you if you have a regular thirty-minute period of focused time. Before you begin your productive time, turn the do not disturb mode “on” on your phone, disconnect from the internet, and remind your colleagues not to bother you for the next thirty minutes. </p>

    <h2>Tips &amp; Tricks</h2>

    <p>I have a few tricks for making the most of my divergent time. I copy and paste many resources into a notes document in preparation for convergent time. My computer has a special hotkey that lets me quickly paste a screenshot into my notes. While collecting these resources, I ensure that I only copy the actionable parts of the document; I don’t want to spend my convergent time scrolling through notes. </p>

    <p>Here’s an example: instead of copying the whole documentation for a library, I screenshot only the functions and methods I plan to use. Not only does this facilitate great productivity time, but it also helps focus my planning on specific steps I must accomplish. </p>

    <figure class="image">
      <img src="https://res.cloudinary.com/boat-shed/image/upload/v1696203109/js-doc_j2mpbx.webp" alt="go documentation of a function in a Golang plackage" />
      <figcaption>An example of selecting only the most important details. This a single function from the Go nats package documentation.</figcaption>
    </figure>

    <h2>Conclusion</h2>

    <p>Finding time to be productive is overwhelming. We cannot rely on our employers to help us find time to do our work. Instead, we need to create a strategy for maximising our distraction-free time. Understanding the <i>creative process</i> and which work mode you are operating in can unlock your creative potential. </p>

    <p>The ideas from this article primarily come from Tiago Forte’s excellent book <a href="https://www.buildingasecondbrain.com/book"><i>Building a Second Brain</i></a>. I highly recommend reading it. For the time-poor, you may also read <a href="/summaries/building-a-second-brain">my summary.</a></p>

  </div>
</article>
