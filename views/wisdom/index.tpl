<h1>Wisdom</h1>

<blockquote cite="https://www.ursulakleguin.com/lao-tzu-the-tao-te-ching">
To attain knowledge, add things every day. To attain wisdom, remove tings every day. 
- Lao Tzu
</blockquote>

<p>
  Wisdom is the lessons I have learned 
  <br />
  from books 
  <br /> 
  and experience 
  <br /> 
  distilled 
  <br /> 
  to the smallest form with meaning.
</p>

<ul>
  <li>
    <a 
      hx-get="/wisdom/distractions"
      hx-push-url="true"
      hx-target="body"
      >
      On Distractions
    </a>
  </li>
</ul>
