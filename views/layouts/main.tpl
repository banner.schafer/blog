
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>A few thoughts</title>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="description" content=""><!-- <link rel="icon" href="favicon.png"> -->
  <link rel="icon" href="/static/bs-logo.svg">

  <script src="/static/htmx.min.js"></script><!-- CSS Reset -->
  <script src="/static/hyper.min.js"></script><!-- CSS Reset -->
  <link rel="stylesheet" href="/static/pico.min.css"><!-- Milligram CSS -->
  <link rel="stylesheet" href="/static/styles.css"><!-- Milligram CSS -->
</head>
<body hx-boost="true">
  <div class="container">
    <nav>
      <ul>
        <li>
          <div style="padding: 2rem 0 2rem 0;">
            <strong>A few thoughts</strong>
            <br />
            <span>In a few words</span>
          </div>

        </li>
      </ul>
      <ul>
        <li>
          <a 
            hx-get="/projects"
            hx-target="body"
            hx-push-url="true"
            >
              Projects
          </a>
        </li>
        <li>
          <a 
            hx-get="/summaries"
            hx-target="body"
            hx-push-url="true"
            >
              Summaries
          </a>
        </li>
        <li>
          <a 
            hx-get="/wisdom"
            hx-target="body"
            hx-push-url="true"
            >
              Wisdom
          </a>
        </li>
        <li>
          <a 
            hx-get="/essays"
            hx-target="body"
            hx-push-url="true"
            >
              Essays
          </a>
        </li>
      </ul>
    </nav>
    <hr />
  </div>
  <main class="container">
    {{embed}}
  </main>
</body>
</html>
