package main

import (
	"os"
	"schafer14/blog/blog"
)

func main() {
	os.Exit(blog.Main())
}
